import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def plotDiffAtTime(time,disp,disp_filt,x,y,Nx,Ny):
    time_a = time
    fig,ax = plt.subplots(1,3,figsize=(20,4.5))
    x_select = np.floor(np.linspace(0,Nx-1,4)).astype(int)
    y_select = np.floor(np.linspace(0,Ny-1,4)).astype(int)

    im0 = ax[0].imshow(disp[time,:,:],cmap="coolwarm",origin="lower")
    cbar0 = plt.colorbar(im0,ax=ax[0])
    cbar0.set_label("Displacement [m]")
    ax[0].set_title("Unfiltered at "+str(time_a)+" s")
    im1 = ax[1].imshow(disp_filt[time_a,:,:],cmap="coolwarm",origin="lower")
    cbar1 = plt.colorbar(im1,ax=ax[1])
    cbar1.set_label("Displacement [m]")
    ax[1].set_title("Filtered at "+str(time_a)+" s")
    ax[1].set_xlabel("x")
    ax[1].set_ylabel("y")
    
    disp_diff = []# reset
    #disp_pad_max  = np.max(np.abs(disp_pad [:,:]))
    #disp_filt_max = np.max(np.abs(disp_filt[:,:]))
    
    disp_diff = (disp[time,:,:]-disp_filt[time_a,:,:])

    im2 = ax[2].imshow(disp_diff[:,:],cmap="coolwarm",origin="lower")
    cbar2 = plt.colorbar(im2,ax=ax[2])
    cbar2.set_label("[m]")
    ax[2].set_title("Absolute Difference at "+str(time_a)+" s")
    ax[2].set_xlabel("x")
    ax[2].set_ylabel("y")

    for i in range(0,3):
        ax[i].set_xlabel("x [m]")
        ax[i].set_ylabel("y [m]")
        ax[i].set_xticks(x_select)
        ax[i].set_xticklabels(x[x_select])
        ax[i].set_yticks(y_select)
        ax[i].set_yticklabels(y[x_select])
        
    plt.show()
                                                                                                    