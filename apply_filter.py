import pandas as pd
import netCDF4 as net
import numpy as np
import matplotlib.pyplot as plt
import pyfftw

from fk_config import *

# restart kernel afterwards and reload
FS = np.load(disp_name+"ft.npy")
meta = np.load(disp_name+"ft-meta.npy",allow_pickle=True)
Nt_pad = meta[0]
Nt = meta[1]
Nx = meta[2]
Ny = meta[3]
t  = meta[4]
x  = meta[5]
y  = meta[6]
Nf = meta[7]
Nw = meta[8]
Nu = meta[9]
f  = meta[10]
w  = meta[11]
u  = meta[12]

FS_filt = []  # reset
FS_filt = FS

mask = getMaskI(u,w,f)
for i in range(Nt_pad):
    FS_filt[i,:,:] = np.multiply(FS[i,:,:],mask)

#Convolve in f
#for j in range(0,Ny):
#    for j in range(0,Nw):
#        FS_filt[:,i,j] = np.convolve(FS_filt[:,i,j],stencil,mode="same")/weight    

#save FS_filt to free memeory again
np.save(disp_name+"ft-filt.npy",FS_filt)    


