import pandas as pd
import netCDF4 as net
import numpy as np
import matplotlib.pyplot as plt
import pyfftw

#path="../Benchmark/DisplacementsAnikoNew/ge73tes2/"
#disp_name = "disp-25km-25-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-25km-50-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-25km-75-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-45km-75-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-45km-50-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-45km-25-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-40km-75-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-40km-50-beach-disp" 
#filter_wave_l = 0.000007

#disp_name = "disp-40km-25-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-30km-75-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-30km-50-beach-disp"
#filter_wave_l = 0.000007

#disp_name = "disp-30km-25-beach-disp"
#filter_wave_l = 0.000007

path="../Benchmark/DisplacementsAnikoNew/ge73tes2/poreo/Dc2_r6/"
disp_name = "displacement-cropped-disp"
filter_wave_l = 0.000014

# path="../Benchmark/DisplacementsAnikoNew/ge73tes2/poreo/Dc3_r10/"
# disp_name = "displacement-cropped-disp"
# filter_wave_l = 0.000014

path="../Benchmark/DisplacementsAnikoNew/ge73tes2/poreo/nu_3/"
disp_name = "displacement-cropped-disp"
filter_wave_l = 0.000014

#numpy implementation
def myfftn(field):
    Nt,Ny,Nx = field.shape
    Nw = Nx     // 2 + 1 
    FS   = np.zeros((Nt,Ny,Nw),dtype=complex)
   
    for i in range(Nt):
        for j in range(Ny):
            FS[i,j,:] = np.fft.rfft(field[i,j,:])
        
    for i in range(Nt):
        for j in range(Nw):
            FS[i,:,j] = np.fft.fft(FS[i,:,j])
            
    for i in range(Ny):
        for j in range(Nw):
            FS[:,i,j] = np.fft.fft(FS[:,i,j])
            
    return FS

def myifftn(FS):
    Nt,Ny,Nw = FS.shape
    Nx = (Nw - 1) * 2
    FS_fu = np.zeros((Nt,Ny,Nw),dtype=complex)
    field=[]
    field = np.zeros((Nt,Ny,Nx))
    for i in range(Nt):
        for j in range(Nw):
            FS_fu[i,:,j] = np.fft.ifft(FS[i,:,j])
            #FS[i,:,j] = pyfftw.interfaces.numpy_fft.ifft(FS[i,:,j],threads=8)
    for i in range(Ny):
        for j in range(Nw):
            FS_fu[:,i,j] = np.fft.ifft(FS_fu[:,i,j])
            #FS[:,i,j] = pyfftw.interfaces.numpy_fft.ifft(FS[:,i,j],threads=8)
    
    for i in range(Nt):
        for j in range(Ny):
            field[i,j,:] = np.fft.irfft(FS_fu[i,j,:])
            #field[i,j,:] = pyfftw.interfaces.numpy_fft.irfft(FS[i,j,:],threads=8)
            
    return field

stencil_length = 10
stencil = np.ones(stencil_length)
weight = sum(stencil)

def getMaskI(u,w,f):
    Ny = len(u)
    Nw = len(w)
    vel = np.sqrt(np.outer(np.ones(Ny),np.multiply(w,w))+np.outer(np.multiply(u,u),np.ones(Nw)))
    vel[abs(vel)==np.inf] = 0

    mask = vel
    mask[:,:] = np.where((vel) < filter_wave_l,np.ones((Ny,Nw)),np.zeros((Ny,Nw)))
    #mask_tmp = np.zeros(mask.shape)
    # pad entries for convolution for now only for x small
    pad_shape = (mask.shape[0] + 2 * stencil_length , mask.shape[1] + stencil_length)
    mask_pad = np.zeros(pad_shape)
    
    pad_start_x = stencil_length
    pad_start_y = stencil_length
    pad_end_y = mask_pad.shape[0]-stencil_length
    
    for i in range(stencil_length):
        mask_pad [pad_start_y:pad_end_y,i] = mask [:,0]

        
    mask_pad[pad_start_y:pad_end_y,stencil_length:] = mask
    mask_pad_temp = mask_pad.copy()
    
    for j in range(pad_start_y,pad_end_y):
        mask_pad_temp[j,:] = np.convolve(mask_pad[j,:],stencil,mode="same") / weight
        
    for i in range(stencil_length):
        mask_pad_temp     [i,pad_start_x:] = mask_pad_temp[pad_start_y,pad_start_x:]
        mask_pad_temp[-(1+i),pad_start_x:] = mask_pad_temp[pad_end_y-1,  pad_start_x:]
        
    for j in range(0,mask_pad.shape[1]):
        mask_pad[:,j] = np.convolve(mask_pad_temp[:,j],stencil,mode="same") / weight

    mask = mask_pad[pad_start_y:pad_end_y ,pad_start_x:]
    
    return mask

def fftrshift(FS):
    a,b = FS.shape
    fac = 0
    if a % 2==1:
        fac = 1
    FS_res = np.zeros(FS.shape)
    FS_res[a//2:,:]   = FS[:a//2 + fac,:]
    FS_res[:a//2 + fac,:]   = FS[a//2:,:]
    return FS_res
