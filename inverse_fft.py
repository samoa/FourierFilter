import pandas as pd
import netCDF4 as net
import numpy as np
import matplotlib.pyplot as plt
import pyfftw

from fk_config import *

FS_filt = np.load(disp_name+"ft-filt.npy")
meta = np.load(disp_name+"ft-meta.npy",allow_pickle=True)
Nt_pad = meta[0]
Nt = meta[1]
Nx = meta[2]
Ny = meta[3]
t  = meta[4]
x  = meta[5]
y  = meta[6]
Nf = meta[7]
Nw = meta[8]
Nu = meta[9]
f  = meta[10]
w  = meta[11]
u  = meta[12]

disp_filt = np.real(myifftn(FS_filt))

#save filtered displacement
np.save(disp_name+"-filt.npy",disp_filt)