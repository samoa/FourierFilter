# FourierFilter

Fourier Filter to erase fast seismic waves from earthquake displacement fields:

To run the filter use the fk_config.py to set all required parameters.

Then run
 ```bash
python prepare_disp.py
python apply_filter.py
python inverse_fft.py
 ```
